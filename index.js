//@ts-check
const fill = document.querySelector(".fill");
const empties = document.querySelectorAll(".empty");

fill.addEventListener("dragstart", dragStart);
fill.addEventListener("dragend", dragEnd);

for (const empty of empties) {
  empty.addEventListener("dragover", dragOver);
  empty.addEventListener("dragenter", dragEnter);
  empty.addEventListener("dragleave", dragLeave);
  empty.addEventListener("drop", dragDrop);
}

function dragStart() {
  console.log("start");
  this.className += " hold";
  setTimeout(() => (this.className = "invisible"), 0);
}

function dragEnd() {
  this.className = "fill";
}

function dragOver(e) {
  e.preventDefault();
}
function dragEnter(e) {
  e.preventDefault();
  this.className += " hovered";
}
function dragLeave() {
  this.className += "empty";
}
function dragDrop() {
  this.className += "empty";
  this.append(fill);
}

window.onload = function() {
  for (var i = 0; i < 10; i++) {
    for (var j = 0; j < 10; j++) {
      var d = document.createElement("div");
      d.className = "box";
      d.className += " " + i+','+j;
      // @ts-ignore
      grid.insertBefore(d, grid.lastChild);
    }
  }
};
